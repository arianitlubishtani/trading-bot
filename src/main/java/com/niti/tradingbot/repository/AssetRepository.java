package com.niti.tradingbot.repository;

import com.niti.tradingbot.model.Asset;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AssetRepository extends JpaRepository<Asset, String> {
}
