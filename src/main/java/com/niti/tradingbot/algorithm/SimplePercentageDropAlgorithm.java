package com.niti.tradingbot.algorithm;

import com.niti.tradingbot.dto.AssetDto;
import com.niti.tradingbot.model.TradingSignal;

public class SimplePercentageDropAlgorithm {

    private static final double THRESHOLD = 0.05; // 5%

    public TradingSignal evaluate(String assetId, String assetName, String currentPriceStr, String price24HrsAgoStr) {
        double currentPrice = Double.parseDouble(currentPriceStr);
        double price24HrsAgo = Double.parseDouble(price24HrsAgoStr);

        double priceChange = (currentPrice - price24HrsAgo) / price24HrsAgo;

        /* Simple strategy
        - If the price has dropped by 5% or more, recommend a SELL.
        - If the price has risen by 5% or more, recommend a BUY.
        - Otherwise, recommend a HOLD. */

        if (priceChange >= THRESHOLD) {
            return new TradingSignal(assetId, assetName, TradingSignal.SignalType.BUY,
                    currentPriceStr, price24HrsAgoStr);
        } else if (priceChange <= -THRESHOLD) {
            return new TradingSignal(assetId, assetName, TradingSignal.SignalType.SELL,
                    currentPriceStr, price24HrsAgoStr);
        } else {
            return new TradingSignal(assetId, assetName, TradingSignal.SignalType.HOLD,
                    currentPriceStr, price24HrsAgoStr);
        }

    }

}
