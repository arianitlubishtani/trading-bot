package com.niti.tradingbot.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.UUID;

@Getter
@Setter
@Entity
@Table(name = "assets")
public class Asset {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "primary_id")
    private Long primaryId;

    @Column(length = 255)
    private String id;

    @Column(name = "created_at", updatable = false)
    private LocalDateTime createdAt;

    @Column
    private String rank;

    @Column
    private String symbol;

    @Column
    private String name;

    @Column
    private String supply;

    @Column(name = "max_supply")
    private String maxSupply;

    @Column(name = "market_cap_usd")
    private String marketCapUsd;

    @Column(name = "volume_usd_24_hr")
    private String volumeUsd24Hr;

    @Column(name = "price_usd")
    private String priceUsd;

    @Column(name = "change_percent_24_hr")
    private String changePercent24Hr;

    @Column(name = "vwap_24_hr")
    private String vwap24Hr;

}
