package com.niti.tradingbot.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class TradingSignal {
        private String assetId;
        private String assetName;
        private SignalType signalType;
        private String currentPrice;
        private String price24HrsAgo;

        public enum SignalType {
            BUY, SELL, HOLD
        }
}
