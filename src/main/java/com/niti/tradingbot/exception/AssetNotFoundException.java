package com.niti.tradingbot.exception;

public class AssetNotFoundException extends RuntimeException{

    public AssetNotFoundException(String message) {
        super(message);
    }
}
