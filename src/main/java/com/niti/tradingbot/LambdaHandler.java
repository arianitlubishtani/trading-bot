package com.niti.tradingbot;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.niti.tradingbot.model.Asset;
import com.niti.tradingbot.repository.AssetRepository;
import com.niti.tradingbot.service.AssetService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.time.LocalDateTime;
import java.util.UUID;

public class LambdaHandler implements RequestHandler<String, String> {

    private ApplicationContext applicationContext;

    public LambdaHandler() {
        // Initialize Spring application context
        applicationContext = new AnnotationConfigApplicationContext(TradingBotApplication.class);
    }
    public String handleRequest(String input, Context context) {

        //AssetService assetService = applicationContext.getBean(AssetService.class);

        // Invoke the method to save assets
        //assetService.saveAllAssets();
        AssetRepository assetRepository = applicationContext.getBean(AssetRepository.class);

        // Manually setting values for the test asset
        Asset testAsset = new Asset();
        testAsset.setId(UUID.randomUUID().toString());
        testAsset.setCreatedAt(LocalDateTime.now());
        testAsset.setRank("1");
        testAsset.setSymbol("TST");
        testAsset.setName("Test Asset");
        testAsset.setSupply("1000000");
        testAsset.setMaxSupply("2000000");
        testAsset.setMarketCapUsd("500000");
        testAsset.setVolumeUsd24Hr("30000");
        testAsset.setPriceUsd("5");
        testAsset.setChangePercent24Hr("2.5");
        testAsset.setVwap24Hr("4.8");

        // Save the test asset
        assetRepository.save(testAsset);

        return "Assets saved successfully";
    }
}