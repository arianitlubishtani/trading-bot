package com.niti.tradingbot.client;

import com.niti.tradingbot.dto.AssetDto;
import com.niti.tradingbot.dto.AssetHistoryResponseDto;
import com.niti.tradingbot.dto.AssetMarketResponseDto;
import com.niti.tradingbot.dto.AssetResponseDto;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Component
public class CoinApiClient {

    private static final String BASE_URL = "https://api.coincap.io/v2";

    private final RestTemplate restTemplate;

    public CoinApiClient(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public List<AssetDto> getAllAssets() {
        AssetResponseDto.AssetsList response = restTemplate.getForObject(BASE_URL + "/assets", AssetResponseDto.AssetsList.class);
        return response.getAssetDtos();
    }

    public AssetDto getAssetById(String id) {
        AssetResponseDto.SingleAsset response = restTemplate.getForObject(BASE_URL + "/assets/" + id, AssetResponseDto.SingleAsset.class);
        return response.getAssetdto();
    }

    public AssetHistoryResponseDto getAssetHistory(String id, String interval) {
        String url = BASE_URL + "/assets/" + id + "/history";
        if (interval != null && !interval.isEmpty()) {
            url += "?interval=" + interval;
        }
        return restTemplate.getForObject(url, AssetHistoryResponseDto.class);
    }

    public AssetMarketResponseDto getAssetMarkets(String id) {
        return restTemplate.getForObject(BASE_URL + "/assets/" + id + "/markets", AssetMarketResponseDto.class);
    }
}
