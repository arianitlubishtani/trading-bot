package com.niti.tradingbot.service;

import com.niti.tradingbot.algorithm.SimplePercentageDropAlgorithm;
import com.niti.tradingbot.dto.AssetDto;
import com.niti.tradingbot.dto.AssetHistoryResponseDto;
import com.niti.tradingbot.enums.TimeInterval;
import com.niti.tradingbot.model.TradingSignal;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TradingService {

    private final AssetService assetService;
    private final SimplePercentageDropAlgorithm tradingAlgorithm;

    public TradingService(AssetService assetService) {
        this.assetService = assetService;
        this.tradingAlgorithm = new SimplePercentageDropAlgorithm();
    }

    public TradingSignal evaluateOpportunity(String assetId) {
        AssetDto currentAsset = assetService.getAssetById(assetId);
        AssetHistoryResponseDto assetHistory = assetService.getAssetHistory(assetId, TimeInterval.D1.getInterval());

        String price24HrsAgoStr = extractPriceFrom24HrsAgo(assetHistory);

        return tradingAlgorithm.evaluate(assetId, currentAsset.getName(), currentAsset.getPriceUsd(), price24HrsAgoStr);
    }

    private String extractPriceFrom24HrsAgo(AssetHistoryResponseDto assetHistory) {
        if (assetHistory == null || assetHistory.getData() == null || assetHistory.getData().isEmpty()) {
            return null;
        }

        // Assuming the list is ordered by time, and the last entry is the latest,
        // and the second last entry represents data from 24 hours ago.
        AssetHistoryResponseDto.AssetHistory history24HrsAgo =
                assetHistory.getData().get(assetHistory.getData().size() - 2);

        return history24HrsAgo.getPriceUsd();

    }
}
