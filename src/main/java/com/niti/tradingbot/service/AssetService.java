package com.niti.tradingbot.service;

import com.niti.tradingbot.client.CoinApiClient;
import com.niti.tradingbot.dto.AssetDto;
import com.niti.tradingbot.dto.AssetHistoryResponseDto;
import com.niti.tradingbot.dto.AssetMarketResponseDto;
import com.niti.tradingbot.exception.AssetNotFoundException;
import com.niti.tradingbot.model.Asset;
import com.niti.tradingbot.repository.AssetRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class AssetService {

    private final CoinApiClient cryptoClient;
    private final AssetRepository assetRepository;

    @Autowired
    public AssetService(CoinApiClient cryptoClient, AssetRepository assetRepository) {
        this.cryptoClient = cryptoClient;
        this.assetRepository = assetRepository;
    }

    public List<AssetDto> getAllAssets() {
        return cryptoClient.getAllAssets();
    }

    public List<AssetDto> saveAllAssets() {
        List<AssetDto> assetDtos = cryptoClient.getAllAssets();

        if(assetDtos.isEmpty()) {
            throw new AssetNotFoundException("No assets found.");
        }

        List<Asset> assets = assetDtos.stream()
                .map(this::convertToEntity)
                .collect(Collectors.toList());

        assetRepository.saveAll(assets);

        return assetDtos;
    }

    private Asset convertToEntity(AssetDto assetDto) {
        Asset asset = new Asset();

        asset.setId(assetDto.getId());
        asset.setCreatedAt(LocalDateTime.now());
        asset.setRank(assetDto.getRank());
        asset.setSymbol(assetDto.getSymbol());
        asset.setName(assetDto.getName());
        asset.setSupply(assetDto.getSupply());
        asset.setMaxSupply(assetDto.getMaxSupply());
        asset.setMarketCapUsd(assetDto.getMarketCapUsd());
        asset.setVolumeUsd24Hr(assetDto.getVolumeUsd24Hr());
        asset.setPriceUsd(assetDto.getPriceUsd());
        asset.setChangePercent24Hr(assetDto.getChangePercent24Hr());
        asset.setVwap24Hr(assetDto.getVwap24Hr());

        return asset;
    }

    public AssetDto getAssetById(String id) {
        return cryptoClient.getAssetById(id);
    }

    public AssetHistoryResponseDto getAssetHistory(String id, String interval) {
        return cryptoClient.getAssetHistory(id, interval);
    }

    public AssetMarketResponseDto getAssetMarkets(String id) {
        return cryptoClient.getAssetMarkets(id);
    }
}
