package com.niti.tradingbot.controller;

import com.niti.tradingbot.dto.AssetDto;
import com.niti.tradingbot.dto.AssetHistoryResponseDto;
import com.niti.tradingbot.dto.AssetMarketResponseDto;
import com.niti.tradingbot.exception.AssetNotFoundException;
import com.niti.tradingbot.service.AssetService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/asset")
public class AssetController {

    private final AssetService assetService;

    public AssetController(AssetService assetService) {
        this.assetService = assetService;
    }

    @GetMapping("")
    public ResponseEntity<List<AssetDto>> getAllAssets() {
        List<AssetDto> assets = assetService.getAllAssets();
        if (assets.isEmpty()) {
            throw new AssetNotFoundException("No assets found.");
        }
        return ResponseEntity.ok(assets);
    }

    @PostMapping("/save")
    public ResponseEntity<List<AssetDto>> saveAssets() {
        List<AssetDto> assets = assetService.saveAllAssets();
        if (assets.isEmpty()) {
            throw new AssetNotFoundException("No assets found.");
        }
        return ResponseEntity.ok(assets);
    }

    @GetMapping("/{id}")
    public ResponseEntity<AssetDto> getAssetById(@PathVariable String id) {
        AssetDto asset = assetService.getAssetById(id);
        if (asset == null) {
            throw new AssetNotFoundException("Asset with ID: " + id + " not found.");
        }
        return ResponseEntity.ok(asset);
    }

    @GetMapping("/{id}/history")
    public ResponseEntity<AssetHistoryResponseDto> getAssetHistory(@PathVariable String id,
                                                                   @RequestParam(required = false) String interval) {
        AssetHistoryResponseDto assetHistory = assetService.getAssetHistory(id, interval);
        if (assetHistory == null) {
            throw new AssetNotFoundException("History for asset with ID: " + id + " not found.");
        }
        return ResponseEntity.ok(assetHistory);
    }

    @GetMapping("/{id}/markets")
    public ResponseEntity<AssetMarketResponseDto> getAssetMarkets(@PathVariable String id) {
        AssetMarketResponseDto assetMarkets = assetService.getAssetMarkets(id);
        if (assetMarkets == null) {
            throw new AssetNotFoundException("Markets for asset with ID: " + id + " not found.");
        }
        return ResponseEntity.ok(assetMarkets);
    }
}
