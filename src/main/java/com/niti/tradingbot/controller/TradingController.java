package com.niti.tradingbot.controller;

import com.niti.tradingbot.model.TradingSignal;
import com.niti.tradingbot.service.TradingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/trading")
public class TradingController {

    private final TradingService tradingService;

    public TradingController(TradingService tradingService) {
        this.tradingService = tradingService;
    }

    @GetMapping("/evaluate/{assetId}")
    public ResponseEntity<TradingSignal> evaluateOpportunity(@PathVariable String assetId) {
        TradingSignal tradingSignal = tradingService.evaluateOpportunity(assetId);
        return ResponseEntity.ok(tradingSignal);
    }
}
