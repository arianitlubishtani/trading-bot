package com.niti.tradingbot;

import com.niti.tradingbot.service.AssetService;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class ScheduledTask {

    private final AssetService assetService;

    public ScheduledTask(AssetService assetService) {
        this.assetService = assetService;
    }

    @Scheduled(cron = "0 * * * * ?", zone = "CET")
    public void performScheduledTask() {
        try {
            //assetService.saveAllAssets();
            System.out.println("Scheduled task executed successfully.");
        } catch (Exception e) {
            System.err.println("Error during scheduled task execution: " + e.getMessage());
        }
    }

}
