package com.niti.tradingbot.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

public class AssetResponseDto {
    @Getter
    @Setter
    public static class AssetsList {
        @JsonProperty("data")
        private List<AssetDto> assetDtos;

        @JsonProperty("timestamp")
        private Long timestamp;
    }

    @Getter
    @Setter
    public static class SingleAsset {
        @JsonProperty("data")
        private AssetDto assetdto;

        @JsonProperty("timestamp")
        private Long timestamp;
    }
}
