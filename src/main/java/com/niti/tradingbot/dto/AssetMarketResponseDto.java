package com.niti.tradingbot.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class AssetMarketResponseDto {

    private List<AssetData> data;

    @Setter
    @Getter
    public static class AssetData {
        private String exchangeId;
        private String baseId;
        private String quoteId;
        private String baseSymbol;
        private String quoteSymbol;
        private double volumeUsd24Hr;
        private double priceUsd;
        private double volumePercent;
    }
}
