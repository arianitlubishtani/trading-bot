package com.niti.tradingbot.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class AssetHistoryResponseDto {

    private List<AssetHistory> data;

    @Getter
    @Setter
    public static class AssetHistory {
        private String priceUsd;
        private long time;
    }
}
